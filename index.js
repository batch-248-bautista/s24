// Template Literals
let getCube = Math.pow(8,3);
console.log(`The cube of 8 is ${getCube}.`);

let address = ['Jones', 'Isabela', 'Cagayan Valley'];

let [town, province, region] = address;
console.log(`I live at ${town}, ${province} ${region}.`);

//object destructuring
let animal= {
	animalName : "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	height: "20 ft 3 in",
}

let {animalName, species, weight, height} = animal;
console.log(`${animalName} was a ${species}. He weighed at ${weight} kgs with a measurement of ${height}.`)


//arrow functions
let numberArr = [1, 2, 3, 4, 5];

numberArr.forEach(x => console.log(x));

let reducedNumber = numberArr.reduce((x,y) => 
	x + y);

console.log(reducedNumber);

function Dog ()
{
	this.name = 'Maviz',
	this.age = '3',
	this.breed = 'American Bully'
}

const dog2 = new Dog();
console.log(dog2);
